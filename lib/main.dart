// ignore_for_file: sort_child_properties_last

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Example Blue Thermal Print'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<BluetoothDevice> devices = [];
  BluetoothDevice? selectedDevices;
  BlueThermalPrinter printer = BlueThermalPrinter.instance;

  @override
  void initState() {
    super.initState();
    getDevice();
  }

  void getDevice() async {
    devices = await printer.getBondedDevices();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              DropdownButton<BluetoothDevice>(
                value: selectedDevices,
                hint: const Text("pilih thermal printer"),
                onChanged: (devices){
                  setState(() {
                    selectedDevices = devices;
                  });
                },
                items: devices
                    .map((e) => DropdownMenuItem(
                  child: Text(e.name!),
                  value: e,
                ))
                    .toList(),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(onPressed: () {
                printer.connect(selectedDevices!);
              }, child: const Text("Connect")),
              ElevatedButton(onPressed: () {
                printer.disconnect();
              }, child: const Text("Connect")),
              ElevatedButton(onPressed: () async {
                if((await printer.isConnected)!){
                  printer.printNewLine();
                  printer.printCustom("thermal printer", 0, 1);
                }


              }, child: const Text("Connect"))

            ],
          ),
        ));
  }
}
